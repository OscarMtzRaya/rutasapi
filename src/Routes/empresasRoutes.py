from app import app, db
from model.model import Empresa, EmpresaSchema
from flask import request, jsonify

empresa_schema = EmpresaSchema()
empresas_schema = EmpresaSchema(many=True)

@app.route('/empresas', methods=['GET'])
def get_empresas():
    all_empresas = Empresa.query.all()
    result = empresas_schema.dump(all_empresas)
    for empresa in result:
        if empresa["EMP_ACT"] == 0:          
            result.remove(empresa)
    return jsonify(result)


@app.route('/empresasInactivos', methods=['GET'])
def get_empresasInactivos():
    all_empresas = Empresa.query.all()
    result = empresas_schema.dump(all_empresas)
    for empresa in result:
        if empresa["EMP_ACT"] == 1:          
            result.remove(empresa)
    return jsonify(result)   

@app.route('/empresas/<id>', methods=['GET'])
def get_empresa(id):
    empresa = Empresa.query.get(id)
    return empresa_schema.jsonify(empresa)

@app.route('/empresas/<id>', methods=['DELETE'])
def delete_empresa(id):
    empresa = empresas.query.get(id)
    db.session.delete(empresa)
    db.session.commit()
    return empresa_schema.jsonify(empresa)

@app.route('/empresas/<id>', methods=['PATCH'])
def update_empresa(id):
    empresa = Empresa.query.get(id)
    if empresa is not None:
        empresa.EMP_ACT = int(not empresa.EMP_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return empresa_schema.jsonify(empresa)

@app.route('/empresas', methods=['POST'])
def create_empresa():
    nom = request.json['EMP_NOM']
    desc = request.json['EMP_DESC']

    new_empresa = Empresa(EMP_NOM=nom, EMP_DESC=desc, EMP_ACT=1)
    db.session.add(new_empresa)
    db.session.commit()
    return empresa_schema.jsonify(new_empresa)