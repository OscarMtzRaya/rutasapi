from app import app, db
from model.model import Rutasparada, RutasparadaSchema
from flask import request, jsonify

rutasparada_schema = RutasparadaSchema()
rutasparadas_schema = RutasparadaSchema(many=True)

@app.route('/rutasparadas', methods=['GET'])
def get_rutasparadas():
    all_rutasparadas = Rutasparada.query.filter(Rutasparada.RP_ACT == 1)
    result = rutasparadas_schema.dump(all_rutasparadas)    
    return jsonify(result)

@app.route('/rutasparadasInactivos', methods=['GET'])
def get_rutasparadasInactivos():
    all_rutasparadas = Rutasparada.query.filter(Rutasparada.RP_ACT == 0)
    result = rutasparadas_schema.dump(all_rutasparadas)             
    return jsonify(result)   

@app.route('/rutasparadas/<id>', methods=['GET'])
def get_Rutasparada(id):
    rutasparada = Rutasparada.query.get(id)
    return rutasparada_schema.jsonify(rutasparada)

@app.route('/rutasparadas/<id>', methods=['DELETE'])
def delete_Rutasparada(id):
    rutasparada = Rutasparada.query.get(id)
    db.session.delete(rutasparada)
    db.session.commit()
    return rutasparada_schema.jsonify(rutasparada)

@app.route('/rutasparadas/<id>', methods=['PATCH'])
def update_Rutasparada(id):
    rutasparada = Rutasparada.query.get(id)
    if rutasparada is not None:
        rutasparada.RP_ACT = int(not Rutasparada.RP_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return rutasparada_schema.jsonify(rutasparada)

@app.route('/rutasparadas', methods=['POST'])
def create_Rutasparada():
    return 'rutasparada_schema.jsonify(new_Rutasparada)'