from app import app, db
from model.model import Ruta, RutaSchema
from flask import request, jsonify

ruta_schema = RutaSchema()
rutas_schema = RutaSchema(many=True)

@app.route('/rutas', methods=['GET'])
def get_Rutas():
    all_Rutas = Ruta.query.filter(Ruta.RUT_ACT == 1)
    result = rutas_schema.dump(all_Rutas)    
    return jsonify(result)

@app.route('/rutasInactivos', methods=['GET'])
def get_RutasInactivos():
    all_Rutas = Ruta.query.filter(Ruta.RUT_ACT == 0)
    result = rutas_schema.dump(all_Rutas)             
    return jsonify(result)   

@app.route('/rutas/<id>', methods=['GET'])
def get_Ruta(id):
    ruta = Ruta.query.get(id)
    return ruta_schema.jsonify(ruta)

@app.route('/rutas/<id>', methods=['DELETE'])
def delete_Ruta(id):
    ruta = Ruta.query.get(id)
    db.session.delete(ruta)
    db.session.commit()
    return ruta_schema.jsonify(ruta)

@app.route('/rutas/<id>', methods=['PATCH'])
def update_Ruta(id):
    ruta = Ruta.query.get(id)
    if ruta is not None:
        ruta.RUT_ACT = int(not ruta.RUT_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return ruta_schema.jsonify(ruta)

@app.route('/rutas', methods=['POST'])
def create_Ruta():
    return 'ruta_schema.jsonify(new_Ruta)'