from app import app, db
from model.model import Horario, HorarioSchema
from flask import request, jsonify

horario_schema = HorarioSchema()
horarios_schema = HorarioSchema(many=True)

@app.route('/horarios', methods=['GET'])
def get_Horarios():
    all_Horarios = Horario.query.filter(Horario.HOR_ACT == 1)
    result = horarios_schema.dump(all_Horarios)    
    return jsonify(result)

@app.route('/horariosInactivos', methods=['GET'])
def get_HorariosInactivos():
    all_Horarios = Horario.query.all()
    result = horarios_schema.dump(all_Horarios)
    for horario in result:
        if horario["HOR_ACT"] == 1:          
            result.remove(horario)
    return jsonify(result)   

@app.route('/horarios/<id>', methods=['GET'])
def get_Horario(id):
    horario = Horario.query.get(id)
    return horario_schema.jsonify(horario)

@app.route('/horarios/<id>', methods=['DELETE'])
def delete_Horario(id):
    horario = Horario.query.get(id)
    db.session.delete(horario)
    db.session.commit()
    return horario_schema.jsonify(horario)

@app.route('/horarios/<id>', methods=['PATCH'])
def update_Horario(id):
    horario = Horario.query.get(id)
    if horario is not None:
        horario.HOR_ACT = int(not horario.HOR_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return horario_schema.jsonify(horario)