from app import app, db
from model.model import Tarifa, TarifaSchema
from flask import request, jsonify

tarifa_schema = TarifaSchema()
tarifas_schema = TarifaSchema(many=True)

@app.route('/tarifas', methods=['GET'])
def get_Tarifas():
    all_Tarifas = Tarifa.query.filter(Tarifa.TAR_ACT == 1)
    result = tarifas_schema.dump(all_Tarifas)    
    return jsonify(result)

@app.route('/tarifasInactivas', methods=['GET'])
def get_TarifasInactivos():
    all_Tarifas = Tarifa.query.filter(Tarifa.TAR_ACT == 0)
    result = tarifas_schema.dump(all_Tarifas)             
    return jsonify(result)   

@app.route('/tarifas/<id>', methods=['GET'])
def get_Tarifa(id):
    tarifa = db.session.query(Tarifa).filter(Tarifa.TAR_ACT == 1)
    return tarifa_schema.jsonify(tarifa)

@app.route('/tarifas/<id>', methods=['DELETE'])
def delete_Tarifa(id):
    tarifa = Tarifa.query.get(id)
    db.session.delete(tarifa)
    db.session.commit()
    return tarifa_schema.jsonify(tarifa)

@app.route('/tarifas/<id>', methods=['PATCH'])
def update_Tarifa(id):
    tarifa = Tarifa.query.get(id)
    if tarifa is not None:
        tarifa.TAR_ACT = int(not tarifa.TAR_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return tarifa_schema.jsonify(tarifa)

@app.route('/tarifas', methods=['POST'])
def create_Tarifa():
    return 'tarifa_schema.jsonify(new_Tarifa)'