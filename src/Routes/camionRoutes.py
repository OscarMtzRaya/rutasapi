from app import app, db
from model.model import Camion, CamionSchema
from flask import request, jsonify

camion_schema = CamionSchema()
camiones_schema = CamionSchema(many=True)

@app.route('/camiones', methods=['GET'])
def get_Camions():
    all_Camions = Camion.query.filter(Camion.CAM_ACT == 1)
    result = camiones_schema.dump(all_Camions)    
    return jsonify(result)

@app.route('/camionesInactivos', methods=['GET'])
def get_CamionsInactivos():
    all_Camions = Camion.query.filter(Camion.CAM_ACT == 0)
    result = camiones_schema.dump(all_Camions)             
    return jsonify(result)   

@app.route('/camiones/<id>', methods=['GET'])
def get_Camion(id):
    camion = Camion.query.get(id)
    return camion_schema.jsonify(camion)

@app.route('/camiones/<id>', methods=['DELETE'])
def delete_Camion(id):
    camion = Camion.query.get(id)
    db.session.delete(camion)
    db.session.commit()
    return camion_schema.jsonify(camion)

@app.route('/camiones/<id>', methods=['PATCH'])
def update_Camion(id):
    camion = Camion.query.get(id)
    if camion is not None:
        camion.CAM_ACT = int(not camion.CAM_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return camion_schema.jsonify(camion)

@app.route('/camiones', methods=['POST'])
def create_Camion():
    return 'camion_schema.jsonify(new_Camion)'