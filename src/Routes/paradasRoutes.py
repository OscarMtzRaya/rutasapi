from app import app, db
from model.model import Parada, ParadaSchema
from flask import request, jsonify

parada_schema = ParadaSchema()
paradas_schema = ParadaSchema(many=True)

@app.route('/paradas', methods=['GET'])
def get_Paradas():
    all_Paradas = Parada.query.all()
    result = paradas_schema.dump(all_Paradas)
    for parada in result:
        if parada["PAR_ACT"] == 0:
            result.remove(parada)
    return jsonify(result)

@app.route('/paradasInactivos', methods=['GET'])
def get_ParadasInactivos():
    all_Paradas = Parada.query.all()
    result = paradas_schema.dump(all_Paradas)
    for parada in result:
        if parada["PAR_ACT"] == 1:
            result.remove(parada)
    return jsonify(result)   

@app.route('/paradas/<id>', methods=['GET'])
def get_Parada(id):
    parada = Parada.query.get(id)
    return parada_schema.jsonify(parada)

@app.route('/paradas/<id>', methods=['DELETE'])
def delete_Parada(id):
    parada = Parada.query.get(id)
    db.session.delete(parada)
    db.session.commit()
    return parada_schema.jsonify(parada)

@app.route('/paradas/<id>', methods=['PATCH'])
def update_Parada(id):
    parada = Parada.query.get(id)
    if parada is not None:
        parada.PAR_ACT = int(not parada.PAR_ACT)
        db.session.commit()
    else:
        return jsonify({"message": "No such resource found", "reason": "Incorrect ID"})
    return parada_schema.jsonify(parada)

@app.route('/paradas', methods=['POST'])
def create_parada():
    nom = request.json['PAR_NOM']
    desc = request.json['PAR_DESC']
    act = 1
    lat = 0
    lon = 0
    
    if request.json['PAR_LAT'] is not None:
        lat = request.json['PAR_LAT']
    if request.json['PAR_LON'] is not None:
        lon = request.json['PAR_LON']
    
    new_parada = Parada(PAR_NOM=nom, PAR_DESC=desc, PAR_ACT=act, PAR_LAT=lat, PAR_LON=lon)
    db.session.add(new_parada)
    db.session.commit()
    return parada_schema.jsonify(new_parada)