from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

app = Flask(__name__)
app.config.from_pyfile('config.py')

db = SQLAlchemy(app)
ma = Marshmallow(app)
CORS(app)

from Routes.mainroute import *
from Routes.empresasRoutes import *
from Routes.horariosRoutes import *
from Routes.paradasRoutes import *
from Routes.tarifasRoutes import *
from Routes.camionRoutes import *
from Routes.rutasRoutes import *
from Routes.rutasparadasRoutes import *

if __name__ == '__main__':
    app.run(debug=True)