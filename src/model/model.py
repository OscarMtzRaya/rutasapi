# coding: utf-8
from sqlalchemy import Column, Float, ForeignKey, Integer, String, Table, Time
from sqlalchemy.dialects.mysql import YEAR
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
from app import db, ma

class Camion(db.Model):
    __tablename__ = 'camion'

    CAM_ID = db.Column(db.Integer, primary_key=True)
    CAM_PLAC = db.Column(db.String(20), nullable=False)
    CAM_MOD = db.Column(db.String(100), nullable=False)
    CAM_ANIO = db.Column(YEAR, nullable=False)
    CAM_ACT = db.Column(db.Integer, nullable=False)
    EMP_ID = db.Column(db.ForeignKey('empresas.EMP_ID'), nullable=False, index=True)

    empresa = db.relationship('Empresa', primaryjoin='Camion.EMP_ID == Empresa.EMP_ID', backref='camions')
    
    def __init__(self, CAM_PLAC, CAM_MOD, CAM_ANIO, CAM_ACT, EMP_ID):
        self.CAM_PLAC = CAM_PLAC
        self.CAM_MOD = CAM_MOD
        self.CAM_ANIO = CAM_ANIO
        self.EMP_ACT = CAM_ACT
        self.EMP_ID = EMP_ID

class CamionSchema(ma.Schema):
    class Meta:
        fields = ('CAM_ID', 'CAM_PLAC', 'CAM_MOD', 'CAM_ANIO', 'CAM_ACT', 'EMP_ID')

class Empresa(db.Model):
    __tablename__ = 'empresas'

    EMP_ID = db.Column(db.Integer, primary_key=True)
    EMP_NOM = db.Column(db.String(150), nullable=False)
    EMP_DESC = db.Column(db.String(250))
    EMP_ACT = db.Column(db.Integer, nullable=False)
    
    def __init__(self, EMP_NOM, EMP_DESC, EMP_ACT):        
        self.EMP_NOM = EMP_NOM
        self.EMP_DESC = EMP_DESC
        self.EMP_ACT = EMP_ACT

class EmpresaSchema(ma.Schema):
    class Meta:
        fields = ('EMP_ID', 'EMP_NOM', 'EMP_DESC', 'EMP_ACT')

class Horario(db.Model):
    __tablename__ = 'horarios'

    HOR_ID = db.Column(db.Integer, primary_key=True)
    HOR_INI = db.Column(db.Time, nullable=False)
    HOR_FIN = db.Column(db.Time, nullable=False)
    HOR_ACT = db.Column(db.Integer, nullable=False)
    HOR_TIPO = db.Column(db.Integer)

    def __init__(self, HOR_INI, HOR_FIN, HOR_ACT, HOR_TIPO):
        self.HOR_INI = HOR_INI
        self.HOR_FIN = HOR_FIN
        self.HOR_ACT = HOR_ACT
        self.HOR_TIPO = HOR_TIPO

class HorarioSchema(ma.Schema):
    class Meta:
        fields = ('HOR_ID', 'HOR_INI', 'HOR_FIN', 'HOR_ACT', 'HOR_TIPO')

class Parada(db.Model):
    __tablename__ = 'paradas'

    PAR_ID = db.Column(db.Integer, primary_key=True)
    PAR_NOM = db.Column(db.String(150), nullable=False, unique=True)
    PAR_DESC = db.Column(db.String(250), nullable=False)
    PAR_ACT = db.Column(db.Integer, nullable=False)
    PAR_LAT = db.Column(db.Float)
    PAR_LON = db.Column(db.Float)

    def __init__(self, PAR_NOM, PAR_DESC, PAR_ACT, PAR_LAT, PAR_LON):
        self.PAR_NOM = PAR_NOM
        self.PAR_DESC = PAR_DESC
        self.PAR_ACT = PAR_ACT
        self.PAR_LAT = PAR_LAT
        self.PAR_LON = PAR_LON

class ParadaSchema(ma.Schema):
    class Meta:
        fields = ('PAR_ID', 'PAR_NOM', 'PAR_DESC', 'PAR_ACT', 'PAR_LAT', 'PAR_LON')

class Ruta(db.Model):
    __tablename__ = 'rutas'

    RUT_ID = db.Column(db.Integer, primary_key=True)
    RUT_NUM = db.Column(db.Integer, nullable=False)
    RUT_TIPO = db.Column(db.String(45), nullable=False)
    HOR_ID = db.Column(db.ForeignKey('horarios.HOR_ID'), nullable=False, index=True)
    CAM_ID = db.Column(db.ForeignKey('camion.CAM_ID'), nullable=False, index=True)
    TAR_ID = db.Column(db.ForeignKey('tarifas.TAR_ID'), nullable=False, index=True)
    RUT_ACT = db.Column(db.Integer, nullable=False)

    camion = db.relationship('Camion', primaryjoin='Ruta.CAM_ID == Camion.CAM_ID', backref='rutas')
    horario = db.relationship('Horario', primaryjoin='Ruta.HOR_ID == Horario.HOR_ID', backref='rutas')
    tarifa = db.relationship('Tarifa', primaryjoin='Ruta.TAR_ID == Tarifa.TAR_ID', backref='rutas')
    
    def __init__(self, RUT_NUM, RUT_TIPO, HOR_ID, CAM_ID, TAR_ID, RUT_ACT):
        self.RUT_NUM = RUT_NUM
        self.RUT_TIPO = RUT_TIPO
        self.HOR_ID = HOR_ID
        self.CAM_ID = CAM_ID
        self.TAR_ID = TAR_ID
        self.RUT_ACT = RUT_ACT
        
class RutaSchema(ma.Schema):
    class Meta:
        fields = ('RUT_ID', 'RUT_NUM', 'RUT_TIPO', 'HOR_ID', 'CAM_ID', 'TAR_ID', 'RUT_ACT')

class Rutasparada(db.Model):
    __tablename__ = 'rutasparadas'

    RP_ID = db.Column(db.Integer, primary_key=True)
    RUT_ID = db.Column(db.ForeignKey('rutas.RUT_ID'), nullable=False, index=True)
    RP_POS = db.Column(db.Integer, nullable=False)
    RP_DIR = db.Column(db.Integer, nullable=False)
    PAR_ID = db.Column(db.ForeignKey('paradas.PAR_ID'), nullable=False, index=True)
    RP_ACT = db.Column(db.Integer, nullable=False)

    parada = db.relationship('Parada', primaryjoin='Rutasparada.PAR_ID == Parada.PAR_ID', backref='rutasparadas')
    ruta = db.relationship('Ruta', primaryjoin='Rutasparada.RUT_ID == Ruta.RUT_ID', backref='rutasparadas')
    
    def __init__(self, RUT_ID, RP_POS, RP_DIR, PAR_ID, RP_ACT):
        self.RUT_ID = RUT_ID
        self.RP_POS = RP_POS
        self.RP_DIR = RP_DIR
        self.PAR_ID = PAR_ID
        self.RP_ACT = RP_ACT

class RutasparadaSchema(ma.Schema):
    class Meta:
        fields = ('RP_ID', 'RUT_ID', 'RP_POS', 'RP_DIR', 'PAR_ID', 'RP_ACT')

class Tarifa(db.Model):
    __tablename__ = 'tarifas'

    TAR_ID = db.Column(db.Integer, primary_key=True)
    TAR_NORM = db.Column(db.Float)
    TAR_EST = db.Column(db.Float)
    TAR_TER = db.Column(db.Float)
    TAR_NIN = db.Column(db.Float)
    TAR_ACT = db.Column(db.Integer)    
    
    def __init__(self, TAR_NORM, TAR_EST, TAR_NIN, TAR_ACT):
        self.TAR_NORM = TAR_NORM
        self.TAR_EST = TAR_EST
        self.TAR_NIN = TAR_NIN
        self.TAR_ACT = TAR_ACT
    
class TarifaSchema(ma.Schema):
    class Meta:
        fields = ('TAR_ID', 'TAR_NORM', 'TAR_EST', 'TAR_NIN', 'TAR_ACT')